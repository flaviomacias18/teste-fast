﻿using System.Collections.Generic;
using Desafio.Models;

namespace Desafio.Repository
{
    public interface IProductRepository
    {
        Product Find(int id);

        IList<Product> All();

        Product Update(Product product);
    }
}