﻿using System.Collections.Generic;
using System.Linq;
using Desafio.Models;

namespace Desafio.Repository
{
    public class ProductRepository : IProductRepository
    {
        public IList<Product> Products => new List<Product>
        {
            new Product {Id = 1, Name = "Notebook", Value = 3199.00M, Stock = 2},
            new Product {Id = 2, Name = "Celular", Value = 850.50M, Stock = 5},
            new Product {Id = 3, Name = "Monitor", Value = 420.10M, Stock = 8},
            new Product {Id = 4, Name = "Refrigerador", Value = 1250.99M, Stock = 3},
            new Product {Id = 5, Name = "Microondas", Value = 350.00M, Stock = 4}
        };

        public Product Find(int id)
        {
            var productDb = Products.SingleOrDefault(x => x.Id == id);

            return productDb;
        }

        public IList<Product> All()
        {
            return Products.ToList();
        }

        public Product Update(Product product)
        {
            var productDb = Products.SingleOrDefault(x => x.Id == product.Id);

            if (productDb == null) return null;

            productDb.Value = product.Value;
            productDb.Stock = product.Stock;

            return productDb;
        }
    }
}