﻿using System;
using System.Net;
using System.Web.Http;
using Desafio.Models;
using Desafio.Repository;

namespace Desafio.Controllers
{
    public class ProductsController : ApiController
    {
        public ProductsController()
        {
            ProductRepository = new ProductRepository();
        }

        public IProductRepository ProductRepository { get; set; }

        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(ProductRepository.All());
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.BadRequest, "ERRO: " + e.Message);
            }
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var product = ProductRepository.Find(id);

                if (product == null)
                    return Content(HttpStatusCode.BadRequest, "Não encontramos nenhum produto com o Id: " + id);

                return Ok(new { product.Id, product.Value, product.Stock});
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.BadRequest, "ERRO: " + e.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult Alter(Product product)
        {
            try
            {
                var productDb = ProductRepository.Update(product);

                if (productDb == null)
                    return Content(HttpStatusCode.BadRequest, "Não encontramos nenhum produto com o Id: " + product.Id);

                return Ok(productDb);
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.BadRequest, "ERRO: " + e.Message);
            }
        }
    }
}